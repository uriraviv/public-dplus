#include "LuaBinding.h"

#include <windows.h>
#include <iostream>
#include <fcntl.h>
#include <io.h>

bool bOpen = false;

void DPlus::Scripting::OpenConsole() {
	static const WORD MAX_CONSOLE_LINES = 5000;
	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;
	AllocConsole();
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

	// redirect unbuffered STDOUT to the console
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen( hConHandle, "w" );
	*stdout = *fp;
	*stderr = *fp;
	// This isn't actually line buffered in win32, it's the same as _IOFBF (see https://msdn.microsoft.com/en-us/library/86cebhfs.aspx).
	// As such, I'll add a bunch of flushes at points in the code.
	setvbuf( stdout, NULL, _IOLBF, 1024 );
	setvbuf( stderr, NULL, _IONBF, 0);

	std::ios::sync_with_stdio();
	//std::cout << "This is a test\n";
	bOpen = true;
}

void DPlus::Scripting::CloseConsole() {
	FreeConsole();
	bOpen = false;
}

bool DPlus::Scripting::IsConsoleOpen() {
	return bOpen;
}
