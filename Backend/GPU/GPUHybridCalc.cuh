#pragma once

#include "GPUInterface.h"

#define BLOCK_WIDTH 32
#define BLOCK_HEIGHT 16


class GPUHybridCalculator : public IGPUGridCalculator
{
public:
		
	virtual bool Initialize(int gpuID, const std::vector<float>& qPoints,
		long long totalSize, int thetaDivisions, int phiDivisions, int qLayers,
		double qMax, double stepSize, GridWorkspace& res);

	virtual bool FreeWorkspace(GridWorkspace& workspace);

	virtual bool ComputeIntensity(std::vector<GridWorkspace>& workspaces,
		double *outData, double epsi, long long iterations,
		progressFunc progfunc = NULL, void *progargs = NULL, float progmin = 0., float progmax = 0., int *pStop = NULL);

	virtual bool SetNumChildren(GridWorkspace &workspace, int numChildren);

	virtual bool AddRotations(GridWorkspace &workspace, std::vector<float4>& rotations);

	virtual bool AddTranslations(GridWorkspace &workspace, int rotationIndex, std::vector<float4>& translations);

	int AssembleAmplitudeGrid(GridWorkspace& workspace, double **subAmp,
		double **subInt, double **transRot, int numSubAmps);
	
	int CalculateSplines(GridWorkspace& workspace);

	int OrientationAverageMC(GridWorkspace& workspace, long long maxIters,
						double convergence,  double *qVals, double *iValsOut);
};
