Read-me

Some .state files include paths to .lua and .pdb files.
The paths are absolute - not relative, therefore when transferring them please ensure you change the paths as well.
If not, when loaded into D+, the software will alert you that the files have not been found.